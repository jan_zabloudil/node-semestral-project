import { app } from './src/app.js'

const server = app.listen(3000, () => {
  console.log('App listening on port 3000')
})

