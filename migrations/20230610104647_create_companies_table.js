/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function (knex) {
  await knex.schema.createTable('companies', (table) => {
    table.increments('id')
    table.string('email').notNullable().unique()
    table.string('name').notNullable()
    table.string('salt').notNullable()
    table.string('password_hash').notNullable()
    table.string('token').notNullable()
    table.string('website')
    table.string('about')
  })

}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function (knex) {
  await knex.schema.dropTable('companies')
}
