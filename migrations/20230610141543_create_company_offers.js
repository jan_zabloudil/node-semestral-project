/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function (knex) {
  await knex.schema.createTable('companies_offers', (table) => {
    table.increments('id')
    table.string('name').notNullable()
    table.string('description').notNullable()
    table.integer('user_id').unsigned()
    table.foreign('user_id').references('companies.id')
  })

}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function (knex) {
  await knex.schema.dropTable('companies_offers')
}
