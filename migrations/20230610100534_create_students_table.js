/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function (knex) {
  await knex.schema.createTable('students', (table) => {
    table.increments('id')
    table.string('email').notNullable().unique()
    table.string('name').notNullable()
    table.string('salt').notNullable()
    table.string('password_hash').notNullable()
    table.string('token').notNullable()
    table.string('linkedin')
    table.string('school')
    table.string('faculty')
    table.string('specialization')
    table.string('grade')

  })

}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function (knex) {
  await knex.schema.dropTable('students')
}
