/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function (knex) {
  await knex.schema.createTable('companies_applications', (table) => {
    table.increments('id')
    table.string('message').notNullable()
    table.integer('offer_id').unsigned()
    table.foreign('offer_id').references('students_offers.id')
    table.integer('user_id').unsigned()
    table.foreign('user_id').references('companies.id')
  })

}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function (knex) {
  await knex.schema.dropTable('companies_applications')
}
