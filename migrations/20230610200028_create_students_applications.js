/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function (knex) {
  await knex.schema.createTable('students_applications', (table) => {
    table.increments('id')
    table.string('message').notNullable()
    table.integer('offer_id').unsigned()
    table.foreign('offer_id').references('companies_offers.id')
    table.integer('user_id').unsigned()
    table.foreign('user_id').references('students.id')
  })

}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function (knex) {
  await knex.schema.dropTable('students_applications')
}
