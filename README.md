# Next steps

- Public company/user profile
- Enable to edit company/user profile
- Show if application for offer was already sent in offer list
- Add created_at, updated_at to offer and application entity
- When validation fails, keep fields in the form filled

