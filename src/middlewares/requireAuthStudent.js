export default async (req, res, next) => {
  
  if(res.locals.user_type != "students"){
    res.redirect('/')
    return
  }

  if(! res.locals.user){
    res.redirect('/')
    return
  }

  next()
}