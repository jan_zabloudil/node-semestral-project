import { getUserByToken } from '../database/sharedUsers.js'
import { usersTypesNames } from '../../config.js'


export default async (req, res, next) => {
  
  const user_type = req.cookies.user_type
  const token = req.cookies.token

  if(! usersTypesNames.includes(user_type)){

    res.locals.user_type = null
    res.locals.user = null
    next()

    return
  }
  
  const user = await getUserByToken(user_type, token)

  if(! user){

    res.locals.user_type = null
    res.locals.user = null
    next()

    return
  }

  res.locals.user = user
  res.locals.user_type = user_type
  next()

}