export default async (req, res, next) => {
  
  if(res.locals.user_type != "companies"){
    res.redirect('/')
    return
  }

  if(! res.locals.user){
    res.redirect('/')
    return
  }

  next()
}