import { db } from '../database.js'

export const createOffer = async (entity, name, description, user_id) => {

  const [offer] = await db(entity).insert({ name, description, user_id }).returning('*')

  return offer
}

export const getOffers = async (entity, userEntity) => {

  const offers = await db(entity)
            .select(
                `${entity}.id AS offer_id`,
                `${entity}.name AS offer_name`,
                `${entity}.description AS offer_description`,
                `${userEntity}.name AS user_name`,
            )
            .join(userEntity, `${userEntity}.id`, `${entity}.user_id`)

  return offers
}

export const getOffersByUserId = async (entity, user_id) => {
  const offers = await db(entity)
    .select('*')
    .where({user_id})

  return offers
}

export const getOfferByIdAndUserId = async (entity, offer_id, user_id) => {
  const offer = await db(entity).select('*').where({
    'id': offer_id,
    'user_id': user_id
  }).first()

  return offer
}

export const updateOfferByIdAndUserId = async (entity, offer_id, user_id, name, description) => {
  const offer = await db(entity).where({
    'id': offer_id,
    'user_id': user_id
  }).update({
    name,
    description
  })

  return offer
}

export const deleteOfferByIdAndUserId = async (entity, offer_id, user_id) => {

  await db(entity).delete().where({
    'id': offer_id,
    'user_id': user_id
  })

}