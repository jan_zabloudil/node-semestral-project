import { db } from '../database.js'

export const createApplication = async (entity, message, offer_id, user_id) => {

  const [application] = await db(entity).insert({ message, offer_id, user_id }).returning('*')

  return application
}

export const getApplicationsByUserId = async (entity, offerEntity, userEntity, user_id) => {

  const key = `${entity}.user_id`

  const applications = await db(entity)
      .select(
        `${entity}.message AS message`,
        `${offerEntity}.name AS offer_name`,
        `${userEntity}.name AS user_name`,
      )
      .join(offerEntity, `${offerEntity}.id`, `${entity}.offer_id`)
      .join(userEntity, `${userEntity}.id`, `${entity}.user_id`)
      .where({ [key]: user_id })

  return applications
}

export const getIncomingApplicationsByUserId = async (entity, offerEntity, userEntity, user_id) => {

  const key = `${offerEntity}.user_id`

  const applications = await db(entity)
      .select(
        `${entity}.message AS message`,
        `${offerEntity}.name AS offer_name`,
        `${userEntity}.name AS user_name`,
      )
      .join(offerEntity, `${offerEntity}.id`, `${entity}.offer_id`)
      .join(userEntity, `${userEntity}.id`, `${entity}.user_id`)
      .where({ [key]: user_id })

  return applications
}

