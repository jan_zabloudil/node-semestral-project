import crypto from 'node:crypto'
import { db } from '../database.js'

export const createCompany = async (email, name, password, website, about) => {

  const salt = crypto.randomBytes(16).toString('hex')
  const password_hash = crypto.pbkdf2Sync(password, salt, 100000, 64, 'sha512').toString('hex')
  const token = crypto.randomBytes(16).toString('hex')

  const [company] = await db('companies').insert({ email, name, salt, password_hash, token, website, about }).returning('*')

  return company
}