import crypto from 'node:crypto'
import { db } from '../database.js'

export const getUserByToken = async (entity, token) => {

  const user = await db(entity).where({ token }).first()

  return user
}

export const getUserByPassword = async (entity, email, password) => {

  const user = await db(entity).where({ email }).first()

  if (!user) return null

  const hash = crypto.pbkdf2Sync(password, user.salt, 100000, 64, 'sha512').toString('hex')
  if (hash !== user.password_hash) return null

  return user
}

export const isEmailTaken = async (entity, email) => {

  const user = await db(entity).where({ email }).first()

  if (!user) return false

  return true
}