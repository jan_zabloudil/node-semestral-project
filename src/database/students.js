import crypto from 'node:crypto'
import { db } from '../database.js'

export const createStudent = async (
  email, 
  name, 
  password,
  linkedin,
  school,
  faculty,
  specialization,
  grade
) => {

  const salt = crypto.randomBytes(16).toString('hex')
  const password_hash = crypto.pbkdf2Sync(password, salt, 100000, 64, 'sha512').toString('hex')
  const token = crypto.randomBytes(16).toString('hex')

  const [student] = await db('students').insert({ 
    email, 
    name, 
    salt, 
    password_hash, 
    token,
    linkedin,
    school,
    faculty,
    specialization,
    grade

  }).returning('*')

  return student
}