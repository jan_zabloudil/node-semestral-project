import express from 'express'
import cookieParser from 'cookie-parser'
import ejsExtend from 'express-ejs-extend';

import { router as studentsRouter } from './routes/students.js'
import { router as companiesRouter } from './routes/companies.js'
import { router as studentsOffersRouter } from './routes/studentsOffers.js'
import { router as companiesOffersRouter } from './routes/companiesOffers.js'
import { router as studentsOffersApplicationsRouter } from './routes/studentsOffersApplications.js'
import { router as companiesOffersApplicationsRouter } from './routes/companiesOffersApplications.js'
import { router as defaultRouter } from './routes/default.js'
import loadUser from './middlewares/loadUser.js'

export const app = express()
app.use(express.static('public'))

app.engine('ejs', ejsExtend);
app.set('view engine', 'ejs')

app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

app.use(loadUser)

app.use(defaultRouter)
app.use(studentsRouter)
app.use(studentsOffersRouter)
app.use(studentsOffersApplicationsRouter)
app.use(companiesRouter)
app.use(companiesOffersRouter)
app.use(companiesOffersApplicationsRouter)

app.use((req, res) => {
  res.status(404).render('error404')
})