import express from 'express'
import { createApplication } from '../database/applications.js'
import requireAuthCompany from '../middlewares/requireAuthCompany.js'

export const router = express.Router()

router.post('/firma/odeslat-zajem/:id', requireAuthCompany, async (req, res) => {

  if(! req.body.message) return res.redirect('/firma/nabidky?formError=Chybí zpráva pro studenta')

  const offer_id = Number(req.params.id)

  await createApplication('companies_applications', req.body.message, offer_id,  res.locals.user.id)

  res.redirect('/?success=Žádost odeslána')

})