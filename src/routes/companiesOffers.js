import express from 'express'
import requiresAuthCompany from '../middlewares/requireAuthCompany.js'
import { createOffer, getOffers, deleteOfferByIdAndUserId, getOfferByIdAndUserId, updateOfferByIdAndUserId } from './../database/offers.js'
import { companyTypeName } from './../../config.js'

export const router = express.Router()

/*
FRONT OFFICE
*/

router.get('/firma/nabidky', async (req, res) => {

  const formError = req.query.formError;
  const offers = await getOffers('students_offers', 'students')

  return res.render('shared/offers/front/list', {
    'list_title': 'Nabídky pro firmy',
    'route_prefix': 'firma',
    'user_type_allowed_to_send_application': companyTypeName,
    offers,
    formError
  })
  
})

/*
BACK OFFICE
*/

router.get('/firma/nabidky/pridat', requiresAuthCompany, async (req, res) => {

  const formError = req.query.formError

  return res.render('shared/offers/back/add', {
    'route_prefix':'firma',
    formError
  })
})

router.post('/firma/nabidky/pridat', requiresAuthCompany, async (req, res) => {

  if(! req.body.name) return res.redirect('/firma/nabidky/pridat?formError=Chybí jméno')
  if(! req.body.description) return res.redirect('/firma/nabidky/pridat?formError=Chybí popis')

  await createOffer('companies_offers', req.body.name, req.body.description, res.locals.user.id)

  res.redirect('/')
  
})

router.get('/firma/nabidky/:id/smazat', requiresAuthCompany, async (req, res) => {

  const id = Number(req.params.id)

  await deleteOfferByIdAndUserId('companies_offers', id, res.locals.user.id)

  res.redirect('/')
})

router.get('/firma/nabidky/:id/upravit', requiresAuthCompany, async (req, res) => {

  const formError = req.query.formError

  const id = Number(req.params.id)

  const offer = await getOfferByIdAndUserId('companies_offers', id,  res.locals.user.id)

  if(! offer){
    res.status(404).render('error404')
    
    return
  }

  return res.render('shared/offers/back/edit', {
    'route_prefix': 'firma',
    offer,
    formError
  })

})

router.post('/firma/nabidky/:id/upravit', requiresAuthCompany, async (req, res) => {

  if(! req.body.name) return res.redirect('/firma/nabidky/pridat?formError=Chybí jméno')
  if(! req.body.description) return res.redirect('/firma/nabidky/pridat?formError=Chybí popis')

  const id = Number(req.params.id)

  await updateOfferByIdAndUserId('companies_offers', id,  res.locals.user.id, req.body.name, req.body.description)

  res.redirect('/')
})