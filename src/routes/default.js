import express from 'express'
import { getOffersByUserId } from './../database/offers.js'
import { getApplicationsByUserId, getIncomingApplicationsByUserId } from '../database/applications.js'
import { studentTypeName, companyTypeName } from './../../config.js'

export const router = express.Router()

router.get('/', async (req, res) => {

  if(! res.locals.user) return res.render('index')

  if(res.locals.user_type === studentTypeName){

    const success = req.query.success;
    const offers = await getOffersByUserId('students_offers', res.locals.user.id)
    const myApplications = await getApplicationsByUserId('students_applications', 'companies_offers', 'companies', res.locals.user.id)
    const incomingApplications = await getIncomingApplicationsByUserId('companies_applications', 'students_offers', 'companies', res.locals.user.id)

    return res.render('student/index', {
      offers,
      myApplications,
      incomingApplications,
      'route_prefix':'student',
      success
    })
  }

  if(res.locals.user_type === companyTypeName){
    
    const success = req.query.success;
    const offers = await getOffersByUserId('companies_offers', res.locals.user.id)
    const myApplications = await getApplicationsByUserId('companies_applications', 'students_offers', 'students', res.locals.user.id)
    const incomingApplications = await getIncomingApplicationsByUserId('students_applications', 'companies_offers', 'students', res.locals.user.id)

    return res.render('company/index', {
      offers,
      myApplications,
      incomingApplications,
      'route_prefix':'firma',
      success
    })

  }

  res.render('index')
})

router.get('/logout', (req, res) => {

  if(! res.locals.user) return res.redirect('/')

  res.clearCookie('token')
  res.clearCookie('user_type')

  res.redirect('/')
})