import express from 'express'
import { createStudent } from '../database/students.js'
import { getUserByPassword, isEmailTaken } from '../database/sharedUsers.js'
import { studentTypeName } from '../../config.js'

export const router = express.Router()

router.get('/student/registrovat', async (req, res) => {

  if(res.locals.user) return res.redirect('/')

  const formError = req.query.formError;

  return res.render('student/register', { formError })
})

router.post('/student/registrovat', async (req, res) => {

  if(! req.body.email) return res.redirect('/student/registrovat?formError=Chybí email')
  if(! req.body.name) return res.redirect('/student/registrovat?formError=Chybí jméno')
  if(! req.body.password) return res.redirect('/student/registrovat?formError=Chybí heslo')
  if(req.body.password.length < 6) return res.redirect('/student/registrovat?formError=Heslo musí být alespon 6 znaků dlouhé')

  const isEmailUnique = await isEmailTaken('students', req.body.email)

  if(isEmailUnique) return res.redirect('/student/registrovat?formError=Email je již používán')

  const linkedin = (req.body.linkedin ? req.body.linkedin : null)
  const school = (req.body.school ? req.body.school : null)
  const faculty = (req.body.faculty ? req.body.faculty : null)
  const specialization = (req.body.specialization ? req.body.specialization : null)
  const grade = (req.body.grade ? req.body.grade : null)
  
  const student = await createStudent(
    req.body.email, 
    req.body.name, 
    req.body.password, 
    linkedin,
    school,
    faculty,
    specialization,
    grade
  )

  res.cookie('token', student.token)
  res.cookie('user_type', studentTypeName)

  res.redirect('/')
})

router.get('/student/prihlasit', async (req, res) => {
  
  if(res.locals.user) return res.redirect('/')

  const formError = req.query.formError;

  return res.render('shared/login', {
    "route_prefix": "student",
    "title": "Přihlášení pro studenty",
    formError
  })
})

router.post('/student/prihlasit', async (req, res) => {
  
  if(! req.body.email) return res.redirect('/student/prihlasit?formError=Zadejte email')
  if(! req.body.password) return res.redirect('/student/prihlasit?formError=Zadejte heslo')

  const student = await getUserByPassword('students', req.body.email, req.body.password)

  if(! student) return res.redirect('/student/prihlasit?formError=Nesprávné údaje')

  res.cookie('token', student.token)
  res.cookie('user_type', studentTypeName)

  res.redirect('/')
})