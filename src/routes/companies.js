import express from 'express'
import { getUserByPassword, isEmailTaken } from './../database/sharedUsers.js'
import { createCompany } from './../database/companies.js'
import { companyTypeName } from './../../config.js'

export const router = express.Router()

router.get('/firma/registrovat', async (req, res) => {

  if(res.locals.user) return res.redirect('/')

  const formError = req.query.formError;

  return res.render('company/register', {formError})
})

router.post('/firma/registrovat', async (req, res) => {

  if(! req.body.email) return res.redirect('/firma/registrovat?formError=Chybí email')
  if(! req.body.name) return res.redirect('/firma/registrovat?formError=Chybí jméno')
  if(! req.body.password) return res.redirect('/firma/registrovat?formError=Chybí heslo')
  if(req.body.password.length < 6) return res.redirect('/firma/registrovat?formError=Heslo musí být dlouhé aspoň 6 znaků')

  const isEmailUnique = await isEmailTaken('companies', req.body.email)

  if(isEmailUnique) return res.redirect('/firma/registrovat?formError=Email je již používán')

  const website = (req.body.website ? req.body.website : null)
  const about = (req.body.about ? req.body.about : null)

  const company = await createCompany(req.body.email, req.body.name, req.body.password, website, about)

  res.cookie('token', company.token)
  res.cookie('user_type', companyTypeName)

  res.redirect('/')
})

router.get('/firma/prihlasit', async (req, res) => {
  
  if(res.locals.user) return res.redirect('/')

  const formError = req.query.formError;

  return res.render('shared/login', {
    "route_prefix": "firma",
    "title": "Přihlášení pro firmy",
    formError
  })
})

router.post('/firma/prihlasit', async (req, res) => {
  
  if(! req.body.email) return res.redirect('/firma/prihlasit?formError=Zadejte email')
  if(! req.body.password) return res.redirect('/firma/prihlasit?formError=Zadejte heslo')

  const company = await getUserByPassword('companies', req.body.email, req.body.password)

  if(! company) return res.redirect('/firma/prihlasit?formError=Nesprávné údaje')

  res.cookie('token', company.token)
  res.cookie('user_type', companyTypeName)

  res.redirect('/')
})