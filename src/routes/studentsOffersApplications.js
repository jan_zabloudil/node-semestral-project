import express from 'express'
import { createApplication } from '../database/applications.js'
import requireAuthStudent from '../middlewares/requireAuthStudent.js'

export const router = express.Router()

router.post('/student/odeslat-zajem/:id', requireAuthStudent, async (req, res) => {

  if(! req.body.message) return res.redirect('/student/nabidky?formError=Chybí zpráva pro firmu')

  const offer_id = Number(req.params.id)

  await createApplication('students_applications', req.body.message, offer_id,  res.locals.user.id)

  res.redirect('/?success=Žádost odeslána')
})