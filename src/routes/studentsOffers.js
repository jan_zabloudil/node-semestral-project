import express from 'express'
import requireAuthStudent from '../middlewares/requireAuthStudent.js'
import { createOffer, getOffers, deleteOfferByIdAndUserId, getOfferByIdAndUserId, updateOfferByIdAndUserId } from './../database/offers.js'
import { studentTypeName } from './../../config.js'

export const router = express.Router()

/*
FRONT OFFICE
*/

router.get('/student/nabidky', async (req, res) => {

  const formError = req.query.formError;
  const offers = await getOffers('companies_offers', 'companies')

  return res.render('shared/offers/front/list', {
    'list_title': 'Nabídky pro studenty',
    'route_prefix': 'student',
    'user_type_allowed_to_send_application': studentTypeName,
    offers,
    formError
  })
  
})

/*
BACK OFFICE OFFICE
*/

router.get('/student/nabidky/pridat', requireAuthStudent, async (req, res) => {

  const formError = req.query.formError

  return res.render('shared/offers/back/add', {
    'route_prefix':'student',
    formError
  })
})

router.post('/student/nabidky/pridat', requireAuthStudent, async (req, res) => {

  if(! req.body.name) return res.redirect('/student/nabidky/pridat?formError=Chybí jméno')
  if(! req.body.description) return res.redirect('/student/nabidky/pridat?formError=Chybí popis')

  await createOffer('students_offers', req.body.name, req.body.description, res.locals.user.id)

  res.redirect('/')
  
})

router.get('/student/nabidky/:id/smazat', requireAuthStudent, async (req, res) => {

  const id = Number(req.params.id)

  await deleteOfferByIdAndUserId('students_offers', id, res.locals.user.id)

  res.redirect('/')
})

router.get('/student/nabidky/:id/upravit', requireAuthStudent, async (req, res) => {

  const id = Number(req.params.id)
  const formError = req.query.formError

  const offer = await getOfferByIdAndUserId('students_offers', id,  res.locals.user.id)

  if(! offer){
    res.status(404).render('error404')

    return
  }

  return res.render('shared/offers/back/edit', {
    'route_prefix': 'student',
    offer,
    formError
  })

})

router.post('/student/nabidky/:id/upravit', requireAuthStudent, async (req, res) => {

  if(! req.body.name) return res.redirect('/student/nabidky/pridat?formError=Chybí jméno')
  if(! req.body.description) return res.redirect('/student/nabidky/pridat?formError=Chybí popis')

  const id = Number(req.params.id)

  await updateOfferByIdAndUserId('students_offers', id,  res.locals.user.id, req.body.name, req.body.description)

  res.redirect('/')

})