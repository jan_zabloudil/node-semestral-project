export const usersTypesNames = ['students', 'companies']

export const studentTypeName = usersTypesNames[0]

export const companyTypeName = usersTypesNames[1]